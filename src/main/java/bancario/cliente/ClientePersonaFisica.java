package bancario.cliente;

class ClientePersonaFisica implements Cliente{
    private String nombre;
    private Integer dni;

    public ClientePersonaFisica(String nombre, Integer dni) {
        this.setNombre(nombre);
        this.setDni(dni);
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}