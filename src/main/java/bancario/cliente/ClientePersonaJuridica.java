package bancario.cliente;

class ClientePersonaJuridica implements Cliente {
    private String razonSocial;
    private String cuit;

    public String getCuit() {
        return cuit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    @Override
    public String getNombre() {
        return getRazonSocial();
    }
    

}