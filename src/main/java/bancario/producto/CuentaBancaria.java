package bancario.producto;

import java.math.BigDecimal;
import bancario.cliente.*;

class CuentaBancaria {
    private BigDecimal saldo;
    private Cliente cliente;

    public CuentaBancaria() {
        this.setSaldo(new BigDecimal(0.00));
    }

    public CuentaBancaria(Cliente cliente) {
        this.cliente=cliente;
        this.setSaldo(new BigDecimal(0.00));
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

	public void acreditar(BigDecimal importe) {
        this.saldo=this.saldo.add(importe);
	}

	public void debitar(BigDecimal importe) {
        this.saldo=this.saldo.subtract(importe);
	}
}