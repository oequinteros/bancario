package bancario.producto;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import bancario.cliente.*;

public class CuentaBancariaTest {
    @Test 
    public void saldoCeroAlCrearCuentaBancaria (){
        CuentaBancaria cuentaBancaria = new CuentaBancaria();
        assertEquals(cuentaBancaria.getSaldo(),new BigDecimal(0.00));
    }
    @Test 
    public void incrementarSaldoaCuenta (){
        CuentaBancaria cuentaBancaria = new CuentaBancaria();

        cuentaBancaria.acreditar(new BigDecimal(10.00));

        assertEquals(cuentaBancaria.getSaldo(),new BigDecimal(10.00));
    }
    @Test 
    public void decrementarSaldoaCuenta (){
        CuentaBancaria cuentaBancaria = new CuentaBancaria();

        cuentaBancaria.acreditar(new BigDecimal(20.00));
        cuentaBancaria.debitar(new BigDecimal(10.00));

        assertEquals(cuentaBancaria.getSaldo(),new BigDecimal(10.00));
    }

}